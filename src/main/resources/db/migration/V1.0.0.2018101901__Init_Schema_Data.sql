CREATE TABLE tbl_user (
  id bigint(20) NOT NULL,
  username varchar(25) NOT NULL,
  password varchar(75) NOT NULL,
  firstname varchar(75) NOT NULL,
  lastname varchar(75) NOT NULL,
  email varchar(75) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO tbl_user (id, username, password, firstname, lastname,email) VALUES (1, 'manju', '$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu', 'manju', 'nath','manjuc@gmail.com');
INSERT INTO tbl_user (id, username, password,  firstname, lastname,email) VALUES (2, 'sonam', '$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu', 'sonam','kapoor','sakur@gmail.com');
INSERT INTO tbl_user (id, username, password,  firstname, lastname,email) VALUES (3, 'sakura', '$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu', 'sakur','annete','sakur@gmail.com');
INSERT INTO tbl_user (id, username, password,  firstname, lastname,email) VALUES (4, 'naruto', '$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu', 'rue', 'narto','rue@gmail.com');
INSERT INTO tbl_user (id, username, password,  firstname, lastname,email) VALUES (5, 'sakura', '$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu', 'sakur','annete','sakur@gmail.com');


-- hendi / password																																																																																																																																																																																																																																																																																																																																															
-- sasuke / password
-- naruto / password
-- sakura / password
-- manju  /  password