package com.example.jpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * Created by Eclipse Project : spring-boot-security-oauth2-example User:
 * manjugbmsc Email: manjugbmsc@gmail.com Date: 25/12/20 Time: 15.26 To change
 * this template use File | Settings | File Templates.
 */

@Data
@Entity
@Table(name = "tbl_user")
public class User{

	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column
	private String username;

	@Column
	@JsonIgnore
	private String password;

	@Column
	private String firstname;

	@Column
	private String lastname;
	
	@Column
	private String email;
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
/*	public String getusername() {
		return username;
	}

	public void setusername(String username) {
		this.username = username;
	}
	

	

	
	public String getfirstname() {
		return firstname;
	}

	public void setfirstname(String firstname) {
		this.firstname = firstname;
	}
	
	public String getlastname() {
		return lastname;
	}

	public void setlastname(String lastname) {
		this.lastname = lastname;
	}
	
	public String getemail() {
		return email;
	}

	public void setemail(String email) {
		this.email = email;
	}
	
	*/

}
