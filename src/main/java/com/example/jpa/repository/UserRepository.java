package com.example.jpa.repository;

import com.example.jpa.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * Created by Eclipse Project : spring-boot-security-oauth2-example 
 * User: manjugbmsc 
 * Email: manjugbmsc@gmail.com
 *  Date: 25/12/20 Time: 15.26 
 *  To change  * this template use File | Settings | File Templates.
 */

@Repository
@Service
public interface UserRepository extends CrudRepository<User, Long> {
	User findByUsername(String username);
}
