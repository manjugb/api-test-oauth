package com.example.jpa.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

/**
 * Created by Eclipse Project : spring-boot-security-oauth2-example User:
 * manjugbmsc Email: manjugbmsc@gmail.com Date: 25/12/20 Time: 15.26 To change
 * this template use File | Settings | File Templates.
 */

@Configuration
@EnableWebSecurity
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	private static final String RESOURCE_ID = "resource_id";

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) {
		resources.resourceId(RESOURCE_ID).stateless(false);
	}

/*	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.anonymous().disable().authorizeRequests().antMatchers("/users/**").access("hasRole('ADMIN')").and()
				.exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
	}*/
	
	
	/*@Override
	public void configure(HttpSecurity http) throws Exception {
	    http.csrf().disable()
	        .anonymous().and()
	        .authorizeRequests()
	        .antMatchers("/oauth/token").permitAll()
	        .antMatchers(HttpMethod.GET,"/users/**").permitAll();
	}*/
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		//-- define URL patterns to enable OAuth2 security
		http.
		anonymous().disable()
		.requestMatchers().antMatchers("/users/**")
		.and().authorizeRequests()
		.antMatchers("/users/**").access("hasRole('ADMIN') or hasRole('USER')")
		.and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
	}
	
}