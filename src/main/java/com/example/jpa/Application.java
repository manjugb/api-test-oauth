package com.example.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by Eclipse Project : spring-boot-security-oauth2-example User:
 * manjugbmsc Email: manjugbmsc@gmail.com Date: 25/12/20 Time: 15.26 To change
 * this template use File | Settings | File Templates.
 */

@SpringBootApplication
@ComponentScan("com.example") //to scan packages mentioned
@EnableJpaRepositories("com.example")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
